#classification de commentaires de film

import numpy as np
import pandas as pd

df = pd.read_csv('../TextFiles/moviereviews2.tsv', sep='\t')
df.head()

# on vérifie les valeurs nulles
df.isnull().sum()

# on trouve les valeurs nulles et on met les index corrspondant dans une liste blanks 
blanks = []  # start with an empty list

for i,lb,rv in df.itertuples():  
    if type(rv)==str:            
        if rv.isspace():         
            blanks.append(i)    

        
len(blanks)

#On enleve les NaN values
df.dropna(inplace=True)


#Split des data
from sklearn.model_selection import train_test_split

X = df['review']
y = df['label']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)


#Création d'un pipeline

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC

text_clf = Pipeline([('tfidf', TfidfVectorizer()),
                     ('clf', LinearSVC()),
])

# Entrainement du model
text_clf.fit(X_train, y_train)  

# Valeurs prédites
predictions = text_clf.predict(X_test)

# Matrice de confusion
from sklearn import metrics
print(metrics.confusion_matrix(y_test,predictions))
# Rapport classification
print(metrics.classification_report(y_test,predictions))
# Précision
print(metrics.accuracy_score(y_test,predictions))